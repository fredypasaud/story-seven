from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options


class UnitTest(TestCase):

    def test_view(self):
        response = self.client.get('/')
        target = resolve('/')
        self.assertTemplateUsed('index.html')
        self.assertContains(response, 'Current Activities', html=True)
        self.assertContains(response, 'Experiences', html=True)
        self.assertContains(response, 'Achievements', html=True)
        self.assertTrue(target.func, index)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        # firefox
        super(FunctionalTest, self).setUp()
        firefox_options = Options()
        firefox_options.add_argument('--no-sandbox')
        firefox_options.add_argument('--headless')
        firefox_options.add_argument('--disable-gpu')
        self.browser = webdriver.Firefox(firefox_options = firefox_options)

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_web(self):
        self.browser.get('http://127.0.0.1:8000')

        self.assertInHTML('Hello, Welcome', self.browser.page_source)
        self.assertInHTML('Go dark?', self.browser.page_source)
        self.assertInHTML('Current Activities', self.browser.page_source)
        self.assertInHTML('Experiences', self.browser.page_source)
        self.assertInHTML('Achievements', self.browser.page_source)        
