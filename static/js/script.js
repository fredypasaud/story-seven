$(document).ready(function(){

  $(".accordion").click(function(){
    $(".this").toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }

  })


  $("#theme-button").click(function(){
    if($("body").hasClass("go-white")){
      $("body").removeClass("go-white");
      $("body").addClass("go-dark");
      $("button").removeClass("btn-light");
      $("button").addClass("btn-dark");
      $("h1,p,h4").removeClass("text-dark");
      $("h1,p,h4").addClass("text-white");
      $("h4").text("Go white?")
    }
    else{
      $("body").removeClass("go-dark");
      $("body").addClass("go-white");
      $("button").removeClass("btn-dark");
      $("button").addClass("btn-light");
      $("h1,h4,p").removeClass("text-white");
      $("h1,h4,p").addClass("text-dark");
      $("h4").text("Go dark?")
    }
  })
})
